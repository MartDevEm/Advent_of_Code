class BinaryDiagnostic:
    file = open('puzzle', 'r')
    binary_list = [i.replace('\n', '') for i in file]

    def __init__(self):
        self.a = 0
        self.b = 0
        pass

    def power_consumption(self):
        gamma_rate = ''
        epsilon_rate = ''
        for i in range(len(self.binary_list[0])):
            count_ones = 0
            count_zeros = 0
            for j in range(len(self.binary_list)):
                if self.binary_list[j][i] == '1':
                    count_ones += 1
                elif self.binary_list[j][i] == '0':
                    count_zeros += 1
            if count_ones > count_zeros:
                gamma_rate += '1'
                epsilon_rate += '0'
            elif count_ones < count_zeros:
                gamma_rate += '0'
                epsilon_rate += '1'
        return print(f'Result: {int(gamma_rate, 2) * int(epsilon_rate, 2)}\nGamma rate: {gamma_rate} - {int(gamma_rate, 2)}\nEpsilon rate: {epsilon_rate} - {int(epsilon_rate, 2)}')

    def support_rating_oxygen(self, lisst = binary_list, column=1) -> int:
        lisst2 = None
        if len(lisst) > 1:
            for i in range(column):
                count_ones = 0
                count_zeros = 0
                for j in range(len(lisst)):
                    if lisst[j][i] == '1':
                        count_ones += 1
                    elif lisst[j][i] == '0':
                        count_zeros += 1
                if count_ones > count_zeros:
                    lisst2 = [x for x in lisst if x[i] == '1']
                elif count_ones < count_zeros:
                    lisst2 = [x for x in lisst if x[i] == '0']
                elif count_ones == count_zeros:
                    lisst2 = [x for x in lisst if x[i] == '1']
                if len(lisst2) > 1 and column == 12:
                    column = 0
            self.support_rating_oxygen(lisst2, column + 1)
        elif len(lisst) == 1:
            self.a = int(lisst[0], 2)
            print(f'Result support rating oxygen: {lisst} -- {int(lisst[0],2)}')

    def support_rating_CO2(self, lisst=binary_list, column=1) -> int:
        lisst2 = None
        if len(lisst) > 1:
            for i in range(column):
                count_ones = 0
                count_zeros = 0
                for j in range(len(lisst)):
                    if lisst[j][i] == '1':
                        count_ones += 1
                    elif lisst[j][i] == '0':
                        count_zeros += 1
                if count_ones < count_zeros:
                    lisst2 = [x for x in lisst if x[i] == '1']
                elif count_ones > count_zeros:
                    lisst2 = [x for x in lisst if x[i] == '0']
                elif count_ones == count_zeros:
                    lisst2 = [x for x in lisst if x[i] == '0']
                if len(lisst2) > 1 and column == 12:
                    column = 0
            self.support_rating_CO2(lisst2, column + 1)
        elif len(lisst) == 1:
            self.b = int(lisst[0], 2)
            print(f'Result support rating CO2: {lisst} -- {int(lisst[0],2)}')

    def calculate_life_support(self):
        return self.a * self.b


if __name__ == '__main__':

    binary_diagnostic = BinaryDiagnostic()
    binary_diagnostic.power_consumption()
    binary_diagnostic.support_rating_oxygen()
    binary_diagnostic.support_rating_CO2()
    print(f'Result life support: {binary_diagnostic.calculate_life_support()}')
