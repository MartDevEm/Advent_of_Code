class DepthMeasurement:

    file_puzzle = open('puzzle', 'r')
    list_depths = [int(x.replace('\n', '')) for x in file_puzzle]

    def __init__(self):
        pass

    def depth_measure_increase(self):  # Part 1
        return self.calculate_increases(self.list_depths)

    def three_measurement(self):  # Part 2
        start = 0
        end = 3
        three_measurement_list = [sum(self.list_depths[start+i:end+i]) for i in range(len(self.list_depths)) if end + i <= len(self.list_depths)]
        return self.calculate_increases(three_measurement_list)

    def calculate_increases(self, lisst):
        previous_measurement = lisst[0]
        count_increases = 0
        for i in lisst:
            if i > previous_measurement:
                count_increases += 1
                print(f'Increase: {i} - Counter: {count_increases}')
            else:
                print(f'Decreased: {i}')
            previous_measurement = i
        return print(f'Result: {count_increases}\n-------------------------------------------')


if __name__ == '__main__':
    depth_measure = DepthMeasurement()
    depth_measure.depth_measure_increase()
    depth_measure.three_measurement()
