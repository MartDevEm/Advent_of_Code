import numpy as np


class GiantSquid:
    file = open('puzzle', 'r')
    list_bingo_numbers = [int(i) for i in file.readlines()[0].split(',')]
    file.seek(0)  # For being able to read again the same files it is necessary to set the cursor to the start.
    array_5_d = [i.replace('  ', ' ').replace(' ', ',').rstrip('\n').split(',') for i in file.readlines()[1:]
                 if i.rstrip('\n').split(',') != ''
                 and i.replace('  ', ' ').replace(' ', ',').rstrip('\n').split(',') != ['']]
    l2 = [[int(j) for j in i if j != ''] for i in array_5_d]

    def __init__(self):
        pass

    def bingo_subsystem(self, part):
        print(f'Numbers of the bingo subsystem: {self.list_bingo_numbers}')
        print(f'Bingo board: {self.l2}')
        a = len(self.l2) / 5
        print(a)
        print(len(self.array_5_d))
        flag = False  # This flag will break every loop when there is a winner board.
        arr_np = np.zeros((100, 6),
                          dtype=int)  # It is the sum of each column of every board. How many numbers were found in this column.
        arr_np2 = np.zeros((500, 2), dtype=int)  # The sum of every row.
        last_i = 0  # The las number that is related to the winner board.
        sum_unmarked = 0  # The sum of every unmarked number of the winner board.
        bingo_board_winner_position = 0

        for i in self.list_bingo_numbers:
            if flag:
                break
            for j in self.l2:
                if i in j:
                    if arr_np[int(self.l2.index(j) / 5)][5] == 0:  # and arr_np2[self.l2.index(j)][1] == 0:

                        arr_np2[self.l2.index(j)][0] += 1
                        if (self.l2.index(j)) / 5 > int((self.l2.index(j)) / 5):
                            arr_np[int(self.l2.index(j) / 5)][j.index(i)] += 1
                            #  if int(self.l2.index(j) / 5) == 9:  # This line is for testing purpose after knowing the result.
                            #  print(f'i:  {i}')  # It will print every number that was present in the board.
                        if (self.l2.index(j)) / 5 == int((self.l2.index(j)) / 5):
                            arr_np[int(self.l2.index(j) / 5)][j.index(i)] += 1
                            #  if int(self.l2.index(j) / 5) == 9:  # This line is for testing purpose after knowing the result.
                            #  print(f'i: {i}')  # It will print every number that was present in the board.
                        #  j[j.index(i)] = ''  # This line of code generates a bug, in this position of the program. It is again a few lines below.
                        if arr_np2[self.l2.index(j)][0] == 5:  # Row
                            last_i = i
                            if part == 1:
                                print("Row bingo.")
                                flag = True
                            if part == 2:
                                bingo_board_winner_position += 1
                                arr_np[int(self.l2.index(j) / 5)][5] = bingo_board_winner_position
                                arr_np2[self.l2.index(j)][1] = bingo_board_winner_position
                                if arr_np2[self.l2.index(j)][1] == 100:
                                    print("Last board, row bingo.")
                                    flag = True
                        elif 5 in arr_np[int(self.l2.index(j) / 5)][:5]:  # Column
                            last_i = i
                            if part == 1:
                                print("Column bingo.")
                                flag = True
                            if part == 2:
                                bingo_board_winner_position += 1
                                arr_np[int(self.l2.index(j) / 5)][5] = bingo_board_winner_position
                                if arr_np[int(self.l2.index(j) / 5)][5] == 100:
                                    print("Last board, column bingo.")
                                    flag = True
                        j[j.index(i)] = ''  # This is the right place for this line of code.
                        if flag:
                            for x in range(5):
                                print(self.l2[(int(self.l2.index(j) / 5) * 5) + x])
                            sum_unmarked = sum([sum(k for k in self.l2[(int(self.l2.index(j) / 5) * 5) + ii][:] if type(k) == int) for ii in range(5)])
                            print(f'sum_ummarked: {sum_unmarked}\nlast_i: {last_i}\n\nResult: {sum_unmarked * last_i}')
                            break


if __name__ == '__main__':
    giant_squid = GiantSquid()
    giant_squid.bingo_subsystem(2)
