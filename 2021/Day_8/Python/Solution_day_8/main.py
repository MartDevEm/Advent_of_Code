class SevenSegmentSearch:

    def __init__(self):
        self.list_of_segments = []

    def fill_list_from_file(self):
        with open('puzzle', 'r') as file:
            self.list_of_segments = [i.replace('\n', '').split(' | ') for i in file.readlines()]

    def digit_count_part_1(self):
        list_part_1 = [i[1].split(' ') for i in self.list_of_segments]
        counter = 0
        for i in list_part_1:
            for j in i:
                if len(j) == 2 or len(j) == 3 or len(j) == 4 or len(j) == 7:
                    counter += 1
        return counter

    def digit_count_part_2(self):
        list_part_2 = [i[1].split(' ') for i in self.list_of_segments]
        list_part_2_1 = [i[0].split(' ') for i in self.list_of_segments]
        dictionary_segments = {}
        list_5_segment = []
        list_6_segment = []
        counter = 0
        index = 0
        tmp_var_join = ""

        for i in list_part_2_1:
            for j in i:
                if len(j) == 2:
                    dictionary_segments.update({1: j})
                elif len(j) == 3:
                    dictionary_segments.update({7: j})
                elif len(j) == 4:
                    dictionary_segments.update({4: j})
                elif len(j) == 5:
                    list_5_segment.append(j)
                elif len(j) == 6:
                    list_6_segment.append(j)
                elif len(j) == 7:
                    dictionary_segments.update({8: j})

            if len(list_6_segment) > 0:
                for s6 in list_6_segment:
                    if set(dictionary_segments[4]).issubset(set(s6)):
                        dictionary_segments.update({9: s6})
                    elif set(dictionary_segments[1]).issubset(set(s6)):
                        dictionary_segments.update({0: s6})
                    else:
                        dictionary_segments.update({6: s6})

            if len(list_5_segment) > 0:
                for s5 in list_5_segment:
                    if set(dictionary_segments[1]).issubset(set(s5)):
                        dictionary_segments.update({3: s5})
                    elif set(s5).issubset(set(dictionary_segments[9])):
                        dictionary_segments.update({5: s5})
                    else:
                        dictionary_segments.update({2: s5})

            for lp2_2 in list_part_2[index]:
                for ds in dictionary_segments:
                    if set(lp2_2) == set(dictionary_segments[ds]):
                        tmp_var_join += str(ds)

            counter += int(tmp_var_join)
            tmp_var_join = ""
            dictionary_segments = {}
            list_5_segment = []
            list_6_segment = []
            index += 1

        return counter


if __name__ == '__main__':
    seven_segment_search = SevenSegmentSearch()
    seven_segment_search.fill_list_from_file()
    print(f'Result part 1: {seven_segment_search.digit_count_part_1()}')
    print(f'Result part 2: {seven_segment_search.digit_count_part_2()}')



