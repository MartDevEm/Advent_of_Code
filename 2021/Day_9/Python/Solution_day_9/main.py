class SmokeBasin:

    def __init__(self):
        self.list_heights = self.read_file()

    def read_file(self):
        with open('puzzle', 'r') as file:
            return [list(int(j) for j in i.strip('\n')) for i in file.readlines()]

    def low_points(self):
        sum_risk_points = 0
        min_height = 0
        a = 0
        for i in range(len(self.list_heights)):
            for j in range(len(self.list_heights[0])):
                min_height = self.list_heights[i][j]
                if j - 1 >= 0:
                    min_height = min(self.list_heights[i][j-1], min_height)
                if j + 1 < len(self.list_heights[0]):
                    min_height = min(self.list_heights[i][j+1], min_height)
                if i - 1 >= 0:
                    min_height = min(self.list_heights[i-1][j], min_height)
                if i + 1 < len(self.list_heights):
                    min_height = min(self.list_heights[i+1][j], min_height)
                if min_height == self.list_heights[i][j] and min_height != 9:
                    sum_risk_points += min_height + 1
                    #  print(f'Sum: {sum_risk_points}')
                    #  print(f'min: {min_height}')
                    #  print(f'Item: {self.list_heights[i][j]}')
                    #  print(f'Index: i: {i} - j: {j}')
        return sum_risk_points


if __name__ == '__main__':
    smoke_basin = SmokeBasin()
    print(smoke_basin.list_heights)
    print(f'Result part 1: {smoke_basin.low_points()}')
