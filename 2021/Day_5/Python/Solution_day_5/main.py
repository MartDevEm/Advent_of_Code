import numpy as np


class HydrothermalVenture:
    file = open('puzzle', 'r')
    list_vents = [list(map(int, i.rstrip('\n').replace(' -> ', ',').split(','))) for i in file.readlines()]

    def __init__(self):
        self.rows, self.columns = self.get_size_of_matrix()
        self.nump_array_vents = np.zeros((self.rows + 1, self.columns + 1), dtype=int)

    def get_size_of_matrix(self):
        """At the end of the loop we will get the size of the matrix"""
        row_length = 0
        column_length = 0
        x1 = 0
        x2 = 2
        y1 = 1
        y2 = 3
        for i in self.list_vents:
            if i[x1] > i[x2] and i[x1] > row_length:
                row_length = i[x1]
            elif i[x2] > i[x1] and i[x2] > row_length:
                row_length = i[x2]
            elif i[x1] == i[x2] and i[x1] > row_length:
                row_length = i[x1]
            if i[y1] > i[y2] and i[y1] > column_length:
                column_length = i[y1]
            elif i[y2] > i[y1] and i[y2] > column_length:
                column_length = i[y2]
            elif i[y1] == i[y2] and i[y1] > column_length:
                column_length = i[y2]
        print(f'Size matrix: {row_length, column_length}')
        return row_length, column_length

    def vents(self):
        minn = 0  # Gets the lowest of xs or ys.
        difference = 0   # Gets the difference between xs or ys.
        line_overlap = 0
        for i in self.list_vents:
            if i[0] == i[2]:  # Comparing xs, columns.
                minn = min(i[1], i[3])
                difference = max(i[1], i[3]) - minn
                for y in range(difference + 1):
                    self.nump_array_vents[minn + y][i[0]] += 1
            elif i[1] == i[3]:  # Comparing ys, rows.
                minn = min(i[0], i[2])
                difference = max(i[0], i[2]) - minn
                for x in range(difference + 1):
                    self.nump_array_vents[i[1]][minn + x] += 1
            elif i[0] == i[2] and i[1] == i[3]:
                self.nump_array_vents[i[0]][i[1]] += 1
                """Comment from here for just getting the result of the part 1."""
            elif i[0] == i[1] and i[2] == i[3]:  # Comparing diagonals, of type: (1,1) -> (3,3)
                minn = min(i[0], i[2])
                difference = max(i[0], i[2]) - minn
                for xy in range(difference + 1):
                    self.nump_array_vents[minn + xy][minn + xy] += 1
            elif i[0] == i[3] and i[1] == i[2]:  # Comparing diagonals, of type: (9,7) -> (7,9)
                minn = min(i[0], i[2])
                difference = max(i[0], i[2]) - minn
                for xy in range(difference + 1):
                    self.nump_array_vents[max(i[0], i[1]) - xy][minn + xy] += 1
            elif abs(i[0] - i[2]) == abs(i[1] - i[3]):  # Comparing diagonals that do not match the above types.
                min_x = i.index(min(i[0], i[2]))
                max_x = i.index(max(i[0], i[2]))
                difference = i[max_x] - i[min_x]
                if i[min_x + 1] < i[max_x + 1]:
                    for xy in range(difference + 1):
                        self.nump_array_vents[i[min_x + 1] + xy][i[min_x] + xy] += 1
                elif i[min_x + 1] > i[max_x + 1]:
                    for xy in range(difference + 1):
                        self.nump_array_vents[i[min_x + 1] - xy][i[min_x] + xy] += 1
                """Until here"""

        #  for x in self.nump_array_vents:
            #  for xx in x:
                #  if xx > 1:
                    #  line_overlap += 1
        line_overlap = len(sum([k for k in [[j for j in i if j > 1] for i in self.nump_array_vents] if k != []], []))
        return line_overlap


if __name__ == '__main__':
    hydrothermal_venture = HydrothermalVenture()
    print(f'{hydrothermal_venture.list_vents}')
    print(f'Result: {hydrothermal_venture.vents()}')
    # print(hydrothermal_venture.nump_array_vents)
