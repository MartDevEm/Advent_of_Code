import numba as nb
import os
from collections import Counter

class ExecuteLanternFish256:

    file = open('puzzle', 'r')
    timer_list = [list(map(int, i.split(','))) for i in file.readlines()]

    def __init__(self):
        self.counter_list = list(self.count_elements_list(self.timer_list[0]))
        self.counter_dict = self.count_elements_list(self.timer_list[0])

    def count_elements_list(self, array=[]):
        cnt = Counter()
        for i in array:
            cnt[i] += 1
        return cnt

    def start_lantern(self):
        result = 0
        data_from_file = 0
        for i in self.counter_list:
            print(f'ITER: {i}')
            print('Starting...')
            py_prog = " ".join(['python day_6_p_2_2.py', str(i)])
            os.system(py_prog)
            data_from_file = int(self.read_file()[0])
            print(f'>>>>>: {data_from_file}')
            result += int(data_from_file) * int(self.counter_dict.get(int(i)))
            #print(f'Dict-get: {self.counter_dict.get(int(data_from_file))}')
        print(f'Result:  {result}')
    
    def read_file(self):
        with open('/home/artix/data', 'r') as file:
            return file.readlines()



if __name__ == '__main__':
    executeLanternFish256 = ExecuteLanternFish256()
    print(executeLanternFish256.counter_list)
    executeLanternFish256.start_lantern()



