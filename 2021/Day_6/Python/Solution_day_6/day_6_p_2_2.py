import numba as nb
import time
import sys
from collections import Counter

class LanternFish256:

    def __init__(self):
        pass
    @nb.jit()
    def lantern_fish_256_day(self, array=[]):
        start_time = time.time()
        new_fishes = 0
        for day in range(256):
            print(f'Day: {day}')
            for i in range(len(array)):
                if array[i] > 0:
                    array[i] -= 1
                elif array[i] == 0:
                    array[i] = 6
                    new_fishes += 1
            #print('Adding elements...')
            for j in range(new_fishes):
                array.append(8)
            new_fishes = 0
        #print(f'Result: {len(array)}')
        end_time = time.time()
        print(f'Time: {end_time - start_time}')
        self.write_2_file(str(len(array)))

    def write_2_file(self, result):
        print('Writing to file....')
        with open('/home/artix/data','w') as file:
            file.write(result)


if __name__ == '__main__':
    lanternFish256 = LanternFish256()
    lanternFish256.lantern_fish_256_day([int(sys.argv[1])])
