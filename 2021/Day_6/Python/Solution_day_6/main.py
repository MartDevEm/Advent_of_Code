import psutil
import numpy as np
from collections import Counter
import numba as nb

class LanternFish:
    file = open('puzzle', 'r')
    timer_list = [list(map(int, i.split(','))) for i in file.readlines()]

    def __init__(self):
        self.lanternfish_amount = 0

    def number_of_days_until_new_lanternfish(self, number_of_days):
        new_fishes = 0
        for days in range(number_of_days):
            print(days)
            for i in range(len(self.timer_list[0])):
                if self.timer_list[0][i] > 0:
                    self.timer_list[0][i] -= 1
                elif self.timer_list[0][i] == 0:
                    self.timer_list[0][i] = 6
                    new_fishes += 1
            for j in range(new_fishes):
                self.timer_list[0].append(8)
            new_fishes = 0
        print(len(self.timer_list[0]))
    """
    # This piece of code does not work as expected. Fix!
    def number_of_days_until_new_lanternfish_256(self, array=[], days=0, sliced_flag=False, index=0):
        print(f'Days: {days}')
        new_fishes = 0
        out_of_memory_day = days
        for day in range(days):
            print(day)
            for i in range(len(array)):
                if array[i] > 0:
                    array[i] -= 1
                elif array[i] == 0:
                    array[i] = 6
                    new_fishes += 1
            for j in range(new_fishes):
                array.append(8)
            new_fishes = 0
            if psutil.virtual_memory()[2] > 90:
                print(f'Running out of memory. Slicing array...\nDay: {day}.')
                sliced_flag = True
                self.write_array_2_file(day, array[int(len(array)/2):])
                del array[int(len(array)/2):]
                self.number_of_days_until_new_lanternfish_256(array, 256 - day, True)
            elif day == days - 1:
                self.lanternfish_amount += len(array)
                if sliced_flag:
                    print(f'Reading file...')
                    numpy_array = np.loadtxt('/home/user/advent_of_code_test_folder/'+str(out_of_memory_day)+'.txt', dtype=int)
                    array = numpy_array.tolist()
                    print(f'Remaining days: {256 - out_of_memory_day}')
                    self.number_of_days_until_new_lanternfish_256(array, 256 - out_of_memory_day, False)
                elif index == 299:
                    return self.lanternfish_amount
                else:
                    print(f'Next index...')
                    self.number_of_days_until_new_lanternfish_256(self.timer_list[0][index:index+1], 256, False, index+1)
        
    def write_array_2_file(self, file_name, array):
        print(f'Copying array to file...')
        with open('/home/user/advent_of_code_test_folder/'+str(file_name)+'.txt', 'a') as file:
            for i in range(len(array)):
                file.writelines(str(array[i]) + ' ')
    """
    def count_elements_list(self, array=[]):
        #  https://docs.python.org/2/library/collections.html#counter-objects
        cnt = Counter()
        for i in array:
            cnt[i] += 1
        print(f'Result counter: {cnt}')

    @nb.jit()
    def lantern_fish_256_days(self, array=[]):
        new_fishes = 0
        for day in range(256):
            print(day)
            for i in range(len(array)):
                if array[i] > 0:
                    array[i] -= 1
                elif array[i] == 0:
                    array[i] = 6
                    new_fishes += 1
            print('Adding elements...')
            for j in range(new_fishes):
                array.append(8)
            new_fishes = 0
        print(f'Result: {len(array)}')

if __name__ == '__main__':
    lanternFish = LanternFish()
    print(lanternFish.timer_list)
    #lanternFish.number_of_days_until_new_lanternfish(80)
    #print(lanternFish.number_of_days_until_new_lanternfish_256(lanternFish.timer_list[0][:1], 256))
    #lanternFish.count_elements_list(lanternFish.timer_list[0])
    lanternFish.lantern_fish_256_days([1])
    #print(len(lanternFish.timer_list_quarter_1))

