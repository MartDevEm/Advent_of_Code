class Dive:
    file = open('puzzle', 'r')
    list_commands = [(a, int(b)) for a, b, in (x.split() for x in file)]

    def __init__(self):
        pass

    def submarine_movement(self, part):
        forward = 0
        down = 0
        up = 0
        aim = 0
        depth = 0
        for i in self.list_commands:
            if i[0] == 'forward':
                forward += i[1]
                depth += aim * i[1]
            elif i[0] == 'down':
                down += i[1]
                aim += i[1]
            elif i[0] == 'up':
                up += i[1]
                aim = aim - i[1]
        if part == 1:
            return print(f'Part 1:-----\nForward: {forward}\nDown: {down}\nUp: {up}\nResult: {forward * (down - up)}')
        if part == 2:
            return print(f'Part 2:------\nForward: {forward}\nDown: {down}\nUp: {up}\nAim: {aim}\nDepth: {depth}\nResult: {forward * depth}')


if __name__ == '__main__':
    dive = Dive()
    dive.submarine_movement(1)
    dive.submarine_movement(2)
