from collections import Counter
import numba as nb


class TreacheryOfWhales:

    def __init__(self):
        self.lowest_cost_dict = {}

    def open_file(self):
        with open('puzzle', 'r') as file:
            return [list(map(int, i.split(','))) for i in file.readlines()]

    def list_element_counter(self):
        cnt = Counter()
        for i in self.open_file()[0]:
            cnt[i] += 1
        return cnt

    @nb.jit()
    def find_lowest_cost(self, part=0):  # The param part will used for specifying: (day 7, part 1 or 2)
        summ = 0
        count = 0
        minn = 0
        max_in_list = max(self.list_element_counter()) + 1
        for i in range(max_in_list):# self.list_element_counter():
            count += 1
            print(count)
            # print(f'Key: {i} - Value: {self.list_element_counter()[i]}')
            for j in self.list_element_counter():
                if i != j:
                    if part == 1:
                        summ += abs(i - j) * self.list_element_counter()[j]
                    elif part == 2:
                        fuel = 0
                        for k in range(1, (abs(i - j) + 1)):
                            fuel += k
                        summ += fuel * self.list_element_counter()[j]
            self.lowest_cost_dict.update({i: summ})
            summ = 0
        minn = min(self.lowest_cost_dict.values())
        for key, value in self.lowest_cost_dict.items():
            if minn == value:
                print(f'Key: {key} - Value: {value}')
        # return self.lowest_cost_dict


if __name__ == '__main__':

    treachery = TreacheryOfWhales()
    print(treachery.open_file())
    print(len(treachery.open_file()[0]))
    print(treachery.list_element_counter())
    treachery.find_lowest_cost(2)
